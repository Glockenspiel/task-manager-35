package ru.t1.sukhorukova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.data.DataXmlSaveJaxBRequest;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");

        getDomainEndpoint().xmlSaveJaxBData(new DataXmlSaveJaxBRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
