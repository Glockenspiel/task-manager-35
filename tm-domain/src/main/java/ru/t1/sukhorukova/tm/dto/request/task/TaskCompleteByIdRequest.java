package ru.t1.sukhorukova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskCompleteByIdRequest(@Nullable String token) {
        super(token);
    }

}
