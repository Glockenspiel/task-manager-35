package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    public String getSessionKey();

    @NotNull
    public Integer getSessionTimeout();

}
