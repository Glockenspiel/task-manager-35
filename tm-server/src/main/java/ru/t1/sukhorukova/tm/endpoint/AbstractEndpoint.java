package ru.t1.sukhorukova.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.user.NotLoggedInException;
import ru.t1.sukhorukova.tm.exception.user.PermissionException;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.model.User;

import javax.jws.WebService;
import java.nio.file.AccessDeniedException;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final ILocatorService locatorService;

    public AbstractEndpoint(final ILocatorService locatorService) {
        this.locatorService = locatorService;
    }

    protected Session check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        @Nullable final Session session = locatorService.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new PermissionException();
        if(!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new PermissionException();
        return locatorService.getAuthService().validateToken(token);
    }

}
