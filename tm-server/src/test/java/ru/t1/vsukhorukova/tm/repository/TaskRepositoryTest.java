package ru.t1.vsukhorukova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.Comparator;
import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vsukhorukova.tm.constant.TaskTestData.*;
import static ru.t1.vsukhorukova.tm.constant.TaskTestData.USER2_TASK1;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @Test
    public void testTaskAdd() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
    }

    @Test
    public void testTaskAddByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
    }

    @Test
    public void testTaskAddModels() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), repository.findAll().get(1));
    }

    @Test
    public void testTaskSet() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), repository.findAll().get(1));
        repository.set(USER2_TASK_LIST);
        Assert.assertEquals(USER2_TASK_LIST.get(0), repository.findAll().get(0));
    }

    @Test
    public void testTaskFindAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        @NotNull final List<Task> taskList = repository.findAll();
        Assert.assertEquals(USER1_TASK_LIST.get(0), taskList.get(0));
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskList.get(1));
        Assert.assertEquals(USER1_TASK_LIST.get(2), taskList.get(2));
    }

    @Test
    public void testTaskFindAllByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(ADMIN_TASK_LIST);
        @NotNull final List<Task> taskListUser1 = repository.findAll(USER1.getId());
        Assert.assertEquals(USER1_TASK_LIST.get(0), taskListUser1.get(0));
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskListUser1.get(1));
        Assert.assertEquals(USER1_TASK_LIST.get(2), taskListUser1.get(2));
        @NotNull final List<Task> taskListAdmin = repository.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), taskListAdmin.get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), taskListAdmin.get(1));
    }

    @Test
    public void testTaskFindAllComparator() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST.get(2));
        repository.add(USER1_TASK_LIST.get(0));
        repository.add(USER1_TASK_LIST.get(1));
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final Comparator<Task> comparator = sort.getComparator();
        @NotNull final List<Task> taskList = repository.findAll(comparator);
        Assert.assertEquals(USER1_TASK_LIST.get(0), taskList.get(0));
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskList.get(1));
        Assert.assertEquals(USER1_TASK_LIST.get(2), taskList.get(2));
    }

    @Test
    public void testTaskFindAllComparatorByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST.get(2));
        repository.add(ADMIN_TASK_LIST.get(1));
        repository.add(USER1_TASK_LIST.get(0));
        repository.add(ADMIN_TASK_LIST.get(0));
        repository.add(USER1_TASK_LIST.get(1));
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final Comparator<Task> comparator = sort.getComparator();
        @NotNull final List<Task> taskListUser1 = repository.findAll(USER1.getId(), comparator);
        Assert.assertEquals(USER1_TASK_LIST.get(0), taskListUser1.get(0));
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskListUser1.get(1));
        Assert.assertEquals(USER1_TASK_LIST.get(2), taskListUser1.get(2));
        @NotNull final List<Task> taskListAdmin = repository.findAll(ADMIN.getId(), comparator);
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), taskListAdmin.get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), taskListAdmin.get(1));
    }

    @Test
    public void testTaskFindOneById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK2, repository.findOneById(USER1_TASK2.getId()));
        Assert.assertNull(repository.findOneById("test-id"));
    }

    @Test
    public void testTaskFindOneByIdByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(ADMIN_TASK_LIST);
        Assert.assertEquals(USER1_TASK2, repository.findOneById(USER1.getId(), USER1_TASK2.getId()));
        Assert.assertEquals(ADMIN_TASK2, repository.findOneById(ADMIN.getId(), ADMIN_TASK2.getId()));
        Assert.assertNull(repository.findOneById(USER1.getId(), ADMIN_TASK2.getId()));
        Assert.assertNull(repository.findOneById(null, ADMIN_TASK2.getId()));
        Assert.assertNull(repository.findOneById(USER1.getId(), "test-id"));
    }

    @Test
    public void testTaskFindOneByIndex() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK2, repository.findOneByIndex(1));
    }

    @Test
    public void testTaskFindOneByIndexByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(ADMIN_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.get(1), repository.findOneByIndex(USER1.getId(), 1));
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), repository.findOneByIndex(ADMIN.getId(), 0));
        Assert.assertNull(repository.findOneByIndex(null, 0));
    }

    @Test
    public void testTaskRemoveOne() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_TASK2.getId()));
        repository.removeOne(USER1_TASK2);
        Assert.assertNull(repository.findOneById(USER1_TASK2.getId()));
    }

    @Test
    public void testTaskRemoveOneByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(USER2_TASK_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_TASK2.getId()));
        repository.removeOne(USER2.getId(), USER1_TASK2);
        Assert.assertNotNull(repository.findOneById(USER1_TASK2.getId()));
        repository.removeOne(USER1.getId(), USER1_TASK2);
        Assert.assertNull(repository.findOneById(USER1_TASK2.getId()));
    }

    @Test
    public void testTaskRemoveOneById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_TASK2.getId()));
        repository.removeOneById(USER1_TASK2.getId());
        Assert.assertNull(repository.findOneById(USER1_TASK2.getId()));
    }

    @Test
    public void testTaskRemoveOneByIdByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(USER2_TASK_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_TASK2.getId()));
        repository.removeOneById(USER2.getId(), USER1_TASK2.getId());
        Assert.assertNotNull(repository.findOneById(USER1_TASK2.getId()));
        repository.removeOneById(USER1.getId(), USER1_TASK2.getId());
        Assert.assertNull(repository.findOneById(USER1_TASK2.getId()));
    }

    @Test
    public void testTaskRemoveOneByIndex() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK1);
        Assert.assertNotNull(repository.findOneById(USER1_TASK1.getId()));
        repository.removeOneByIndex(0);
        Assert.assertNull(repository.findOneById(USER1_TASK1.getId()));
    }

    @Test
    public void testTaskRemoveOneByIndexByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK1);
        repository.add(USER2_TASK1);
        Assert.assertNotNull(repository.findOneById(USER1_TASK1.getId()));
        Assert.assertNotNull(repository.findOneById(USER2_TASK1.getId()));
        repository.removeOneByIndex(USER1.getId(), 0);
        Assert.assertNotNull(repository.findOneById(USER2_TASK1.getId()));
        Assert.assertNull(repository.findOneById(USER1_TASK1.getId()));
    }

    @Test
    public void testTaskRemoveAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), repository.findAll().size());
        repository.removeAll();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void testTaskRemoveAllByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), repository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_TASK_LIST.size(), repository.findAll(USER2.getId()).size());
        repository.removeAll(USER1.getId());
        Assert.assertEquals(0, repository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_TASK_LIST.size(), repository.findAll(USER2.getId()).size());
    }

    @Test
    public void testTaskGetSize() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), repository.getSize());
    }

    @Test
    public void testTaskGetSizeByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), repository.getSize(USER1.getId()));
        Assert.assertEquals(USER2_TASK_LIST.size(), repository.getSize(USER2.getId()));
    }

    @Test
    public void testTaskExistsById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        Assert.assertTrue(repository.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(repository.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void testTaskExistsByIdByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK_LIST);
        repository.add(USER2_TASK_LIST);
        Assert.assertTrue(repository.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(repository.existsById(USER2.getId(), USER1_TASK1.getId()));
    }

    @Test
    public void testTaskFindAllByProjectId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task1 = USER1_TASK1;
        @NotNull final Task task3 = USER1_TASK3;
        task1.setProjectId(USER1_PROJECT1.getId());
        task3.setProjectId(USER1_PROJECT1.getId());
        repository.add(USER1_TASK_LIST);
        repository.add(USER2_TASK_LIST);
        @NotNull final List<Task> taskList1 = repository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId());
        @NotNull final List<Task> taskList2 = repository.findAllByProjectId(USER2.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_TASK1, taskList1.get(0));
        Assert.assertEquals(USER1_TASK3, taskList1.get(1));
        Assert.assertEquals(0, taskList2.size());
    }

}
