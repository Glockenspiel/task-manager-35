package ru.t1.vsukhorukova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.repository.UserRepository;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @Test
    public void testUserAdd() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findAll().get(0));
    }

    @Test
    public void testUserAddModels() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(USER_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(USER_LIST.get(1), repository.findAll().get(1));
        Assert.assertEquals(USER_LIST.get(2), repository.findAll().get(2));
    }

    @Test
    public void testUserSet() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USER_LIST);
        Assert.assertEquals(USER_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(USER_LIST.get(1), repository.findAll().get(1));
        Assert.assertEquals(USER_LIST.get(2), repository.findAll().get(2));
        repository.set(ADMIN_LIST);
        Assert.assertEquals(ADMIN_LIST.get(0), repository.findAll().get(0));
    }

    @Test
    public void testUserFindAll() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        @NotNull final List<User> userList = repository.findAll();
        Assert.assertEquals(USER_LIST.get(0), userList.get(0));
        Assert.assertEquals(USER_LIST.get(1), userList.get(1));
        Assert.assertEquals(USER_LIST.get(2), userList.get(2));
    }

    @Test
    public void testUserFindOneById() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertNull(repository.findOneById("test-id"));
    }

    @Test
    public void testUserFindOneByIndex() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(USER1, repository.findOneByIndex(1));
    }

    @Test
    public void testUserRemoveOne() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertNotNull(repository.findOneById(USER1.getId()));
        repository.removeOne(USER1);
        Assert.assertNull(repository.findOneById(USER1.getId()));
    }

    @Test
    public void testUserRemoveOneById() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertNotNull(repository.findOneById(USER1.getId()));
        repository.removeOneById(USER1.getId());
        Assert.assertNull(repository.findOneById(USER1.getId()));
    }

    @Test
    public void testUserRemoveOneByIndex() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertNotNull(repository.findOneById(USER1.getId()));
        repository.removeOneByIndex(0);
        Assert.assertNull(repository.findOneById(USER1.getId()));
    }

    @Test
    public void testUserRemoveAll() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(USER_LIST.size(), repository.findAll().size());
        repository.removeAll();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void testUserGetSize() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(USER_LIST.size(), repository.getSize());
    }

    @Test
    public void testUserExistsById() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertTrue(repository.existsById(USER1.getId()));
        Assert.assertFalse(repository.existsById(USER2.getId()));
    }

    @Test
    public void testUserFindByLogin() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        repository.add(ADMIN);
        Assert.assertNull(repository.findByLogin(USER2.getLogin()));
        Assert.assertEquals(USER1.getLogin(), repository.findByLogin(USER1.getLogin()).getLogin());
    }

    @Test
    public void testUserFindByEmail() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        repository.add(ADMIN);
        Assert.assertNull(repository.findByEmail(USER2.getEmail()));
        Assert.assertEquals(USER1.getEmail(), repository.findByEmail(USER1.getEmail()).getEmail());
    }

    @Test
    public void testUserIsLoginExist() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        repository.add(ADMIN);
        Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
    }

    @Test
    public void testUserIsEmailExist() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        repository.add(ADMIN);
        Assert.assertFalse(repository.isEmailExist(USER2.getEmail()));
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
    }

}
