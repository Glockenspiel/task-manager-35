package ru.t1.vsukhorukova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.service.IProjectService;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.service.ProjectService;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.Comparator;
import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.*;
import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void check() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @After
    public void clear() {
        projectRepository.removeAll();
    }

    @Test
    public void testProjectAdd() {
        Assert.assertNotNull(projectService.add(USER1_PROJECT1));
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findAll().get(0));

        thrown.expect(EntityNotFoundException.class);
        projectService.add(NULL_PROJECT);
    }

    @Test
    public void testProjectAddByUserId() {
        Assert.assertNotNull(projectService.add(USER1.getId(), USER1_PROJECT1));
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findAll().get(0));

        thrown.expect(UserIdEmptyException.class);
        projectService.add(NULL_USER_ID, USER1_PROJECT1);
        thrown.expect(EntityNotFoundException.class);
        projectService.add(USER1.getId(), NULL_PROJECT);
    }

    @Test
    public void testProjectAddModels() {
        Assert.assertNotNull(projectService.add(ADMIN_PROJECT_LIST));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), projectRepository.findAll().get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), projectRepository.findAll().get(1));

        thrown.expect(ValueIsNullException.class);
        projectService.add(NULL_PROJECT_LIST);
    }

    @Test
    public void testProjectSet() {
        Assert.assertNotNull(projectService.set(ADMIN_PROJECT_LIST));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), projectRepository.findAll().get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), projectRepository.findAll().get(1));
        Assert.assertNotNull(projectService.set(USER2_PROJECT_LIST));
        Assert.assertEquals(USER2_PROJECT_LIST.get(0), projectRepository.findAll().get(0));

        thrown.expect(ValueIsNullException.class);
        projectService.add(NULL_PROJECT_LIST);
    }

    @Test
    public void testProjectFindAll() {
        projectRepository.add(USER1_PROJECT_LIST);
        @NotNull final List<Project> projectList = projectService.findAll();
        Assert.assertEquals(USER1_PROJECT_LIST.get(0), projectList.get(0));
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectList.get(1));
        Assert.assertEquals(USER1_PROJECT_LIST.get(2), projectList.get(2));
    }

    @Test
    public void testProjectFindAllByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(ADMIN_PROJECT_LIST);
        @NotNull final List<Project> projectListUser1 = projectService.findAll(USER1.getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(0), projectListUser1.get(0));
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectListUser1.get(1));
        Assert.assertEquals(USER1_PROJECT_LIST.get(2), projectListUser1.get(2));
        @NotNull final List<Project> projectListAdmin = projectService.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), projectListAdmin.get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), projectListAdmin.get(1));

        thrown.expect(UserIdEmptyException.class);
        projectService.findAll(NULL_USER_ID);
    }

    @Test
    public void testProjectFindAllComparator() {
        projectRepository.add(USER1_PROJECT_LIST.get(1));
        projectRepository.add(USER1_PROJECT_LIST.get(0));
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final Comparator<Project> comparator = sort.getComparator();
        @NotNull final List<Project> projectList = projectService.findAll(comparator);
        Assert.assertNotNull(comparator);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getName(), projectList.get(0).getName());
        Assert.assertEquals(USER1_PROJECT_LIST.get(1).getName(), projectList.get(1).getName());
    }

    @Test
    public void testProjectFindAllComparatorByUserId() {
        projectRepository.add(ADMIN_PROJECT_LIST.get(1));
        projectRepository.add(USER1_PROJECT_LIST.get(0));
        projectRepository.add(ADMIN_PROJECT_LIST.get(0));
        projectRepository.add(USER1_PROJECT_LIST.get(1));
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final Comparator<Project> comparator = sort.getComparator();
        @NotNull final List<Project> projectListUser1 = projectService.findAll(USER1.getId(), comparator);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getName(), projectListUser1.get(0).getName());
        Assert.assertEquals(USER1_PROJECT_LIST.get(1).getName(), projectListUser1.get(1).getName());
        @NotNull final List<Project> projectListAdmin = projectService.findAll(ADMIN.getId(), comparator);
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0).getName(), projectListAdmin.get(0).getName());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1).getName(), projectListAdmin.get(1).getName());

        thrown.expect(UserIdEmptyException.class);
        projectService.findAll(NULL_USER_ID, comparator);
    }

    @Test
    public void testProjectFindOneById() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2, projectService.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNull(projectService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        projectService.findOneById(NULL_PROJECT_ID);
    }

    @Test
    public void testProjectFindOneByIdByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2, projectService.findOneById(USER1.getId(), USER1_PROJECT2.getId()));
        Assert.assertEquals(ADMIN_PROJECT2, projectService.findOneById(ADMIN.getId(), ADMIN_PROJECT2.getId()));
        Assert.assertNull(projectService.findOneById(USER1.getId(), ADMIN_PROJECT2.getId()));
        Assert.assertNull(projectService.findOneById(USER1.getId(), "test-id"));

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneById(NULL_USER_ID, ADMIN_PROJECT2.getId());
        thrown.expect(IdEmptyException.class);
        projectService.findOneById(NULL_PROJECT_ID);
    }

    @Test
    public void testProjectFindOneByIndex() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2, projectService.findOneByIndex(1));

        thrown.expect(IndexIncorrectException.class);
        projectService.findOneByIndex(-1);
    }

    @Test
    public void testProjectFindOneByIndexByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectService.findOneByIndex(USER1.getId(), 1));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), projectService.findOneByIndex(ADMIN.getId(), 0));

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        projectService.findOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testProjectRemoveOne() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.removeOne(USER1_PROJECT2));
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(EntityNotFoundException.class);
        projectService.removeOne(NULL_PROJECT);
    }

    @Test
    public void testProjectRemoveOneByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(USER2_PROJECT_LIST);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT2.getId()));
        projectService.removeOne(USER2.getId(), USER1_PROJECT2);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT2.getId()));
        projectService.removeOne(USER1.getId(), USER1_PROJECT2);
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOne(NULL_USER_ID, USER1_PROJECT2);
        thrown.expect(EntityNotFoundException.class);
        projectService.removeOne(USER1.getId(), NULL_PROJECT);
    }

    @Test
    public void testProjectRemoveOneById() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.removeOneById(USER1_PROJECT2.getId()));
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(IdEmptyException.class);
        projectService.removeOneById(NULL_PROJECT_ID);
    }

    @Test
    public void testProjectRemoveOneByIdByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(USER2_PROJECT_LIST);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNull(projectService.removeOneById(USER2.getId(), USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.removeOneById(USER1.getId(), USER1_PROJECT2.getId()));
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneById(NULL_USER_ID, USER1_PROJECT2.getId());
        thrown.expect(IdEmptyException.class);
        projectService.removeOneById(USER2.getId(), NULL_PROJECT_ID);
    }

    @Test
    public void testProjectRemoveOneByIndex() {
        projectRepository.add(USER1_PROJECT1);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNotNull(projectService.removeOneByIndex(0));
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));

        thrown.expect(IndexIncorrectException.class);
        projectService.removeOneByIndex(-1);
    }

    @Test
    public void testProjectRemoveOneByIndexByUserId() {
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNotNull(projectRepository.findOneById(USER2_PROJECT1.getId()));
        Assert.assertNotNull(projectService.removeOneByIndex(USER1.getId(), 0));
        Assert.assertNotNull(projectRepository.findOneById(USER2_PROJECT1.getId()));
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        projectService.removeOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testProjectRemoveAll() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll().size());
        projectService.removeAll();
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testProjectRemoveAllByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(USER2_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectRepository.findAll(USER2.getId()).size());
        projectService.removeAll(USER1.getId());
        Assert.assertEquals(0, projectRepository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectRepository.findAll(USER2.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        projectService.removeAll(NULL_USER_ID);
    }

    @Test
    public void testProjectGetSize() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.getSize());
    }

    @Test
    public void testProjectGetSizeByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(USER2_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.getSize(USER1.getId()));
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectService.getSize(USER2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.getSize(NULL_USER_ID);
    }

    @Test
    public void testProjectExistsById() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertTrue(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void testProjectExistsByIdByUserId() {
        projectRepository.add(USER1_PROJECT_LIST);
        projectRepository.add(USER2_PROJECT_LIST);
        Assert.assertTrue(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2.getId(), USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.existsById(NULL_USER_ID, USER1_PROJECT1.getId());
    }

    @Test
    public void testProjectCreate() {
        Assert.assertEquals(0, projectRepository.findAll(USER1.getId()).size());
        @NotNull final String name = "PROJECT_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(projectService.create(USER1.getId(), name, description));
        Assert.assertEquals(1, projectRepository.findAll(USER1.getId()).size());
        @NotNull final Project project = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER1.getId(), project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.create(NULL_USER_ID, name, description);
        thrown.expect(NameEmptyException.class);
        projectService.create(USER1.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        projectService.create(USER1.getId(), name, null);
    }

    @Test
    public void testProjectUpdateById() {
        projectRepository.add(USER1_PROJECT_LIST);
        @NotNull final String name = "PROJECT_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), name, description));
        @NotNull final Project project = projectRepository.findOneById(USER1_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateById(NULL_USER_ID, USER1_PROJECT2.getId(), name, description);
        thrown.expect(ProjectIdEmptyException.class);
        projectService.updateById(USER1.getId(), NULL_PROJECT_ID, name, description);
        thrown.expect(NameEmptyException.class);
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), name, null);
    }

    @Test
    public void testProjectUpdateByIndex() {
        projectRepository.add(USER1_PROJECT_LIST);
        @NotNull final String name = "PROJECT_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(projectService.updateByIndex(USER1.getId(), 1, name, description));
        @NotNull final Project project = projectRepository.findOneByIndex(1);
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateByIndex(NULL_USER_ID, 1, name, description);
        thrown.expect(IndexIncorrectException.class);
        projectService.updateByIndex(USER1.getId(), -1, name, description);
        thrown.expect(NameEmptyException.class);
        projectService.updateByIndex(USER1.getId(), 1, null, description);
        thrown.expect(DescriptionEmptyException.class);
        projectService.updateByIndex(USER1.getId(), 1, name, null);
    }

    @Test
    public void testProjectChangeProjectStatusById() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT1.getStatus(), projectRepository.findOneById(USER1_PROJECT1.getId()).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findOneById(USER1_PROJECT1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusById(NULL_USER_ID, USER1_PROJECT1.getId(), Status.NOT_STARTED);
        thrown.expect(ProjectIdEmptyException.class);
        projectService.changeProjectStatusById(USER1.getId(), NULL_PROJECT_ID, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        projectService.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), null);
    }

    @Test
    public void testProjectChangeProjectStatusByIndex() {
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getStatus(), projectRepository.findOneByIndex(0).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusByIndex(USER1.getId(), 0, Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findOneByIndex(0).getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusByIndex(NULL_USER_ID, 0, Status.NOT_STARTED);
        thrown.expect(IndexIncorrectException.class);
        projectService.changeProjectStatusByIndex(USER1.getId(), -1, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        projectService.changeProjectStatusByIndex(USER1.getId(), 0, null);
    }

}
