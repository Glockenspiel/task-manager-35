package ru.t1.vsukhorukova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.service.ITaskService;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.service.TaskService;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.Comparator;
import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vsukhorukova.tm.constant.TaskTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void check() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @After
    public void clear() {
        taskRepository.removeAll();
    }

    @Test
    public void testTaskAdd() {
        Assert.assertNotNull(taskService.add(USER1_TASK1));
        Assert.assertEquals(USER1_TASK1, taskRepository.findAll().get(0));

        thrown.expect(EntityNotFoundException.class);
        taskService.add(NULL_TASK);
    }

    @Test
    public void testTaskAddByUserId() {
        Assert.assertNotNull(taskService.add(USER1.getId(), USER1_TASK1));
        Assert.assertEquals(USER1_TASK1, taskRepository.findAll().get(0));

        thrown.expect(UserIdEmptyException.class);
        taskService.add(NULL_USER_ID, USER1_TASK1);
        thrown.expect(EntityNotFoundException.class);
        taskService.add(USER1.getId(), NULL_TASK);
    }

    @Test
    public void testTaskAddModels() {
        Assert.assertNotNull(taskService.add(ADMIN_TASK_LIST));
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), taskRepository.findAll().get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), taskRepository.findAll().get(1));

        thrown.expect(ValueIsNullException.class);
        taskService.add(NULL_TASK_LIST);
    }

    @Test
    public void testTaskSet() {
        Assert.assertNotNull(taskService.set(ADMIN_TASK_LIST));
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), taskRepository.findAll().get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), taskRepository.findAll().get(1));
        Assert.assertNotNull(taskRepository.set(USER2_TASK_LIST));
        Assert.assertEquals(USER2_TASK_LIST.get(0), taskRepository.findAll().get(0));

        thrown.expect(ValueIsNullException.class);
        taskService.add(NULL_TASK_LIST);
    }

    @Test
    public void testTaskFindAll() {
        taskRepository.add(USER1_TASK_LIST);
        @NotNull final List<Task> taskList = taskService.findAll();
        Assert.assertEquals(USER1_TASK_LIST.get(0), taskList.get(0));
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskList.get(1));
        Assert.assertEquals(USER1_TASK_LIST.get(2), taskList.get(2));
    }

    @Test
    public void testTaskFindAllByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(ADMIN_TASK_LIST);
        @NotNull final List<Task> taskListUser1 = taskService.findAll(USER1.getId());
        Assert.assertEquals(USER1_TASK_LIST.get(0), taskListUser1.get(0));
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskListUser1.get(1));
        Assert.assertEquals(USER1_TASK_LIST.get(2), taskListUser1.get(2));
        @NotNull final List<Task> taskListAdmin = taskService.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), taskListAdmin.get(0));
        Assert.assertEquals(ADMIN_TASK_LIST.get(1), taskListAdmin.get(1));

        thrown.expect(UserIdEmptyException.class);
        taskService.findAll(NULL_USER_ID);
    }

    @Test
    public void testTaskFindAllComparator() {
        taskRepository.removeAll();
        taskRepository.add(USER1_TASK_LIST.get(1));
        taskRepository.add(USER1_TASK_LIST.get(0));
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final Comparator<Task> comparator = sort.getComparator();
        @NotNull final List<Task> taskList = taskService.findAll(comparator);
        Assert.assertEquals(USER1_TASK_LIST.get(0).getName(), taskList.get(0).getName());
        Assert.assertEquals(USER1_TASK_LIST.get(1).getName(), taskList.get(1).getName());
    }

    @Test
    public void testTaskFindAllComparatorByUserId() {
        taskRepository.removeAll();
        taskRepository.add(ADMIN_TASK_LIST.get(1));
        taskRepository.add(USER1_TASK_LIST.get(0));
        taskRepository.add(ADMIN_TASK_LIST.get(0));
        taskRepository.add(USER1_TASK_LIST.get(1));
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final Comparator<Task> comparator = sort.getComparator();
        @NotNull final List<Task> taskListUser1 = taskService.findAll(USER1.getId(), comparator);
        Assert.assertEquals(USER1_TASK_LIST.get(0).getName(), taskListUser1.get(0).getName());
        Assert.assertEquals(USER1_TASK_LIST.get(1).getName(), taskListUser1.get(1).getName());
        @NotNull final List<Task> taskListAdmin = taskService.findAll(ADMIN.getId(), comparator);
        Assert.assertEquals(ADMIN_TASK_LIST.get(0).getName(), taskListAdmin.get(0).getName());
        Assert.assertEquals(ADMIN_TASK_LIST.get(1).getName(), taskListAdmin.get(1).getName());

        thrown.expect(UserIdEmptyException.class);
        taskService.findAll(NULL_USER_ID, comparator);
    }

    @Test
    public void testTaskFindOneById() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK2, taskService.findOneById(USER1_TASK2.getId()));
        Assert.assertNull(taskService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        taskService.findOneById(NULL_TASK_ID);
    }

    @Test
    public void testTaskFindOneByIdByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(ADMIN_TASK_LIST);
        Assert.assertEquals(USER1_TASK2, taskService.findOneById(USER1.getId(), USER1_TASK2.getId()));
        Assert.assertEquals(ADMIN_TASK2, taskService.findOneById(ADMIN.getId(), ADMIN_TASK2.getId()));
        Assert.assertNull(taskService.findOneById(USER1.getId(), ADMIN_TASK2.getId()));
        Assert.assertNull(taskService.findOneById(USER1.getId(), "test-id"));

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneById(NULL_USER_ID, ADMIN_TASK2.getId());
        thrown.expect(IdEmptyException.class);
        taskService.findOneById(NULL_TASK_ID);
    }

    @Test
    public void testTaskFindOneByIndex() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK2, taskService.findOneByIndex(1));

        thrown.expect(IndexIncorrectException.class);
        taskService.findOneByIndex(-1);
    }

    @Test
    public void testTaskFindOneByIndexByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(ADMIN_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.get(1), taskService.findOneByIndex(USER1.getId(), 1));
        Assert.assertEquals(ADMIN_TASK_LIST.get(0), taskService.findOneByIndex(ADMIN.getId(), 0));

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        taskService.findOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testTaskRemoveOne() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.removeOne(USER1_TASK2));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK2.getId()));

        thrown.expect(EntityNotFoundException.class);
        taskService.removeOne(NULL_TASK);
    }

    @Test
    public void testTaskRemoveOneByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK2.getId()));
        taskService.removeOne(USER2.getId(), USER1_TASK2);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK2.getId()));
        taskService.removeOne(USER1.getId(), USER1_TASK2);
        Assert.assertNull(taskRepository.findOneById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOne(NULL_USER_ID, USER1_TASK2);
        thrown.expect(EntityNotFoundException.class);
        taskService.removeOne(USER1.getId(), NULL_TASK);
    }

    @Test
    public void testTaskRemoveOneById() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.removeOneById(USER1_TASK2.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK2.getId()));

        thrown.expect(IdEmptyException.class);
        taskService.removeOneById(NULL_TASK_ID);
    }

    @Test
    public void testTaskRemoveOneByIdByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK2.getId()));
        Assert.assertNull(taskService.removeOneById(USER2.getId(), USER1_TASK2.getId()));
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.removeOneById(USER1.getId(), USER1_TASK2.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneById(NULL_USER_ID, USER1_TASK2.getId());
        thrown.expect(IdEmptyException.class);
        taskService.removeOneById(USER2.getId(), NULL_TASK_ID);
    }

    @Test
    public void testTaskRemoveOneByIndex() {
        taskRepository.add(USER1_TASK1);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK1.getId()));
        Assert.assertNotNull(taskService.removeOneByIndex(0));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));

        thrown.expect(IndexIncorrectException.class);
        taskService.removeOneByIndex(-1);
    }

    @Test
    public void testTaskRemoveOneByIndexByUserId() {
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK1.getId()));
        Assert.assertNotNull(taskRepository.findOneById(USER2_TASK1.getId()));
        Assert.assertNotNull(taskService.removeOneByIndex(USER1.getId(), 0));
        Assert.assertNotNull(taskRepository.findOneById(USER2_TASK1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        taskService.removeOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testTaskRemoveAll() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAll().size());
        taskService.removeAll();
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testTaskRemoveAllByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_TASK_LIST.size(), taskRepository.findAll(USER2.getId()).size());
        taskService.removeAll(USER1.getId());
        Assert.assertEquals(0, taskRepository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_TASK_LIST.size(), taskRepository.findAll(USER2.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        taskService.removeAll(NULL_USER_ID);
    }

    @Test
    public void testTaskGetSize() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.getSize());
    }

    @Test
    public void testTaskGetSizeByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.getSize(USER1.getId()));
        Assert.assertEquals(USER2_TASK_LIST.size(), taskService.getSize(USER2.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.getSize(NULL_USER_ID);
    }

    @Test
    public void testTaskExistsById() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void testTaskExistsByIdByUserId() {
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertTrue(taskService.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2.getId(), USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.existsById(NULL_USER_ID, USER1_TASK1.getId());
    }

    @Test
    public void testTaskCreate() {
        Assert.assertEquals(0, taskRepository.findAll(USER1.getId()).size());
        @NotNull final String name = "TASK_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(taskService.create(USER1.getId(), name, description));
        Assert.assertEquals(1, taskRepository.findAll(USER1.getId()).size());
        @NotNull final Task task = taskRepository.findOneByIndex(0);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER1.getId(), task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.create(NULL_USER_ID, name, description);
        thrown.expect(NameEmptyException.class);
        taskService.create(USER1.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        taskService.create(USER1.getId(), name, null);
    }

    @Test
    public void testTaskUpdateById() {
        taskRepository.add(USER1_TASK_LIST);
        @NotNull final String name = "TASK_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(taskService.updateById(USER1.getId(), USER1_TASK2.getId(), name, description));
        @NotNull final Task task = taskRepository.findOneById(USER1_TASK2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateById(NULL_USER_ID, USER1_TASK2.getId(), name, description);
        thrown.expect(ProjectIdEmptyException.class);
        taskService.updateById(USER1.getId(), NULL_TASK_ID, name, description);
        thrown.expect(NameEmptyException.class);
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), name, null);
    }

    @Test
    public void testTaskUpdateByIndex() {
        taskRepository.add(USER1_TASK_LIST);
        @NotNull final String name = "TASK_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(taskService.updateByIndex(USER1.getId(), 1, name, description));
        @NotNull final Task task = taskRepository.findOneByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateByIndex(NULL_USER_ID, 1, name, description);
        thrown.expect(IndexIncorrectException.class);
        taskService.updateByIndex(USER1.getId(), -1, name, description);
        thrown.expect(NameEmptyException.class);
        taskService.updateByIndex(USER1.getId(), 1, null, description);
        thrown.expect(DescriptionEmptyException.class);
        taskService.updateByIndex(USER1.getId(), 1, name, null);
    }

    @Test
    public void testTaskChangeTaskStatusById() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK1.getStatus(), taskRepository.findOneById(USER1_TASK1.getId()).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findOneById(USER1_TASK1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusById(NULL_USER_ID, USER1_TASK1.getId(), Status.NOT_STARTED);
        thrown.expect(ProjectIdEmptyException.class);
        taskService.changeTaskStatusById(USER1.getId(), NULL_TASK_ID, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        taskService.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), null);
    }

    @Test
    public void testTaskChangeTaskStatusByIndex() {
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.get(0).getStatus(), taskRepository.findOneByIndex(0).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusByIndex(USER1.getId(), 0, Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findOneByIndex(0).getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusByIndex(NULL_USER_ID, 0, Status.NOT_STARTED);
        thrown.expect(IndexIncorrectException.class);
        taskService.changeTaskStatusByIndex(USER1.getId(), -1, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        taskService.changeTaskStatusByIndex(USER1.getId(), 0, null);
    }

    @Test
    public void testTaskFindAllByProjectId() {
        @NotNull final Task task1 = USER1_TASK1;
        @NotNull final Task task3 = USER1_TASK3;
        task1.setProjectId(USER1_PROJECT1.getId());
        task3.setProjectId(USER1_PROJECT1.getId());
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        @NotNull final List<Task> taskList1 = taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId());
        @NotNull final List<Task> taskList2 = taskService.findAllByProjectId(USER2.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_TASK1, taskList1.get(0));
        Assert.assertEquals(USER1_TASK3, taskList1.get(1));
        Assert.assertEquals(0, taskList2.size());
    }

}
