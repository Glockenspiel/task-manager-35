package ru.t1.vsukhorukova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.UserNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.exception.user.ExistsEmailException;
import ru.t1.sukhorukova.tm.exception.user.ExistsLoginException;
import ru.t1.sukhorukova.tm.exception.user.RoleEmptyException;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.repository.UserRepository;
import ru.t1.sukhorukova.tm.service.PropertyService;
import ru.t1.sukhorukova.tm.service.UserService;
import ru.t1.sukhorukova.tm.util.HashUtil;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void check() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @After
    public void clear() {
        userRepository.removeAll();
        projectRepository.removeAll();
        taskRepository.removeAll();
    }

    @Test
    public void testUserAdd() {
        Assert.assertNotNull(userService.add(USER1));
        Assert.assertEquals(USER1, userRepository.findAll().get(0));

        thrown.expect(EntityNotFoundException.class);
        userService.add(NULL_USER);
    }

    @Test
    public void testUserAddModels() {
        Assert.assertNotNull(userService.add(USER_LIST));
        Assert.assertEquals(USER_LIST.get(0), userRepository.findAll().get(0));
        Assert.assertEquals(USER_LIST.get(1), userRepository.findAll().get(1));
        Assert.assertEquals(USER_LIST.get(2), userRepository.findAll().get(2));

        thrown.expect(ValueIsNullException.class);
        userService.add(NULL_USER_LIST);
    }

    @Test
    public void testUserSet() {
        Assert.assertNotNull(userService.set(USER_LIST));
        Assert.assertEquals(USER_LIST.get(0), userRepository.findAll().get(0));
        Assert.assertEquals(USER_LIST.get(1), userRepository.findAll().get(1));
        Assert.assertEquals(USER_LIST.get(2), userRepository.findAll().get(2));
        userService.set(ADMIN_LIST);
        Assert.assertEquals(ADMIN_LIST.get(0), userRepository.findAll().get(0));

        thrown.expect(ValueIsNullException.class);
        userService.add(NULL_USER_LIST);
    }

    @Test
    public void testUserFindAll() {
        userRepository.add(USER_LIST);
        @NotNull final List<User> userList = userService.findAll();
        Assert.assertEquals(USER_LIST.get(0), userList.get(0));
        Assert.assertEquals(USER_LIST.get(1), userList.get(1));
        Assert.assertEquals(USER_LIST.get(2), userList.get(2));
    }

    @Test
    public void testUserFindOneById() {
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER1, userService.findOneById(USER1.getId()));
        Assert.assertNull(userService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        userService.findOneById(NULL_USER_ID);
    }

    @Test
    public void testUserFindOneByIndex() {
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER1, userService.findOneByIndex(1));

        thrown.expect(IndexIncorrectException.class);
        userService.findOneByIndex(-1);
    }

    @Test
    public void testUserRemoveOne() {
        userRepository.add(USER_LIST);
        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
        Assert.assertNotNull(userService.removeOne(USER1));
        Assert.assertNull(userRepository.findOneById(USER1.getId()));
    }

    @Test
    public void testUserRemoveOneById() {
        userRepository.add(USER1);
        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
        Assert.assertNotNull(userService.removeOneById(USER1.getId()));
        Assert.assertNull(userRepository.findOneById(USER1.getId()));

        thrown.expect(IdEmptyException.class);
        userService.removeOneById(NULL_USER_ID);
    }

    @Test
    public void testUserRemoveOneByIndex() {
        userRepository.add(USER1);
        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
        Assert.assertNotNull(userService.removeOneByIndex(0));
        Assert.assertNull(userRepository.findOneById(USER1.getId()));

        thrown.expect(IndexIncorrectException.class);
        userService.removeOneByIndex(-1);
    }

    @Test
    public void testUserRemoveAll() {
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST.size(), userRepository.findAll().size());
        userService.removeAll();
        Assert.assertEquals(0, userRepository.findAll().size());
    }

    @Test
    public void testUserGetSize() {
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST.size(), userService.getSize());
    }

    @Test
    public void testUserExistsById() {
        userRepository.add(USER1);
        Assert.assertTrue(userService.existsById(USER1.getId()));
        Assert.assertFalse(userService.existsById(USER2.getId()));
    }

    @Test
    public void testUserCreate() {
        Assert.assertEquals(0, userRepository.findAll().size());
        @NotNull final String name = "USER_TEST";
        @NotNull final String password = "password";
        Assert.assertNotNull(userService.create(name, password));
        Assert.assertEquals(1, userRepository.findAll().size());
        @NotNull final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
        Assert.assertEquals(name, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, password), user.getPasswordHash());

        thrown.expect(LoginEmptyException.class);
        userService.create(null, password);
        thrown.expect(ExistsLoginException.class);
        userService.create(name, password);
        thrown.expect(PasswordEmptyException.class);
        userService.create(name, null);
    }

    @Test
    public void testUserCreateEmail() {
        Assert.assertEquals(0, userRepository.findAll().size());
        @NotNull final String name = "USER_TEST";
        @NotNull final String password = "password";
        @NotNull final String email = "user@test.ru";
        Assert.assertNotNull(userService.create(name, password, email));
        Assert.assertEquals(1, userRepository.findAll().size());
        @NotNull final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
        Assert.assertEquals(name, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, password), user.getPasswordHash());
        Assert.assertEquals(email, user.getEmail());

        thrown.expect(LoginEmptyException.class);
        userService.create(null, password, email);
        thrown.expect(ExistsLoginException.class);
        userService.create(name, password, email);
        thrown.expect(PasswordEmptyException.class);
        userService.create(name, null, email);
        thrown.expect(ExistsEmailException.class);
        userService.create("USER_TEST_02", password, email);
    }

    @Test
    public void testUserCreateRole() {
        Assert.assertEquals(0, userRepository.findAll().size());
        @NotNull final String name = "USER_TEST";
        @NotNull final String password = "password";
        @NotNull final Role role = Role.ADMIN;
        Assert.assertNotNull(userService.create(name, password, role));
        Assert.assertEquals(1, userRepository.findAll().size());
        @NotNull final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
        Assert.assertEquals(name, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, password), user.getPasswordHash());

        thrown.expect(LoginEmptyException.class);
        userService.create(null, password, role);
        thrown.expect(ExistsLoginException.class);
        userService.create(name, password, role);
        thrown.expect(PasswordEmptyException.class);
        userService.create(name, null, role);
        thrown.expect(RoleEmptyException.class);
        @Nullable Role roleNull = null;
        userService.create(name, password, roleNull);
    }

    @Test
    public void testUserFindByLogin() {
        userRepository.add(USER1);
        userRepository.add(ADMIN);
        Assert.assertNull(userService.findByLogin(USER2.getLogin()));
        Assert.assertEquals(USER1.getLogin(), userService.findByLogin(USER1.getLogin()).getLogin());
    }

    @Test
    public void testUserFindByEmail() {
        userRepository.add(USER1);
        userRepository.add(ADMIN);
        Assert.assertNull(userService.findByEmail(USER2.getEmail()));
        Assert.assertEquals(USER1.getEmail(), userService.findByEmail(USER1.getEmail()).getEmail());
    }

    @Test
    public void testUserRemoveOneByLogin() {

    }

    @Test
    public void testUserRemoveOneByEmail() {

    }

    @Test
    public void testUserSetPassword() {

    }

    @Test
    public void testUserUpdateUser() {

    }

    @Test
    public void testUserLockOneByLogin() {

    }

    @Test
    public void testUserUnlockOneByLogin() {

    }

    @Test
    public void testUserIsLoginExist() {
        userRepository.add(USER1);
        userRepository.add(ADMIN);
        Assert.assertFalse(userService.isLoginExist(USER2.getLogin()));
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
    }

    @Test
    public void testUserIsEmailExist() {
        userRepository.add(USER1);
        userRepository.add(ADMIN);
        Assert.assertFalse(userService.isEmailExist(USER2.getEmail()));
        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
    }

}
